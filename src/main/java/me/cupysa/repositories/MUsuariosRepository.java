package me.cupysa.repositories;

import me.cupysa.entities.MUsuarios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MUsuariosRepository extends JpaRepository<MUsuarios, Integer> {

}