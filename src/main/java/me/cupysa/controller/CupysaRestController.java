package me.cupysa.controller;

import me.cupysa.custom.DateFormatted;
import me.cupysa.encryption.EncryptionWrapper;
import me.cupysa.encryption.Endpoints;
import me.cupysa.entities.*;
import me.cupysa.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class CupysaRestController {

    /*Links Medium
     * https://medium.com/@salisuwy/building-a-spring-boot-rest-api-a-php-developers-view-part-i-6add2e794646
     *https://medium.com/@salisuwy/building-a-spring-boot-rest-api-part-ii-7ff1e4384b0b
     *https://medium.com/@salisuwy/building-a-spring-boot-rest-api-part-iii-integrating-mysql-database-and-jpa-81391404046a
     * */
    @Autowired
    MUsuariosRepository mUsuariosRepository;

    @GetMapping("/")
    public String status() {
        return "The RESTful API is up and running!";
    }

    @GetMapping("/usuarios")
    public List<MUsuarios> indexUsuario() {
        return mUsuariosRepository.findAll();
    }
}
