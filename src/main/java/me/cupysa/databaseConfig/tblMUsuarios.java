package me.cupysa.databaseConfig;

public class tblMUsuarios {
    public static final String tableName = "musuarios";

    public static final String id = "IDUSUARIO";
    public static final String nombreUsuario = "NOMBRE_USUARIO";
    public static final String apellidoNombres = "APELLIDO_NOMBRES";
    public static final String panterior = "PANTERIOR";
    public static final String fallidos = "FALLIDOS";
    public static final String nivel = "NIVEL";
    public static final String contrasenia = "CONTRASENIA";
    public static final String fechaExpira = "FECHAEXPIRA";
    public static final String estado = "ESTADO";
    public static final String panteriorf = "PANTERIORF";
    public static final String contraseniaf = "CONTRASENIAF";
    public static final String direccion = "DIRECCION";
    public static final String audit = "AUDIT";
    public static final String mail = "MAIL";
    public static final String aviso = "AVISO";
    public static final String permisosEspeciales = "PERMISOSESPECIALES";
    public static final String claveCaja = "CLAVE_CAJA";

}
