package me.cupysa.custom;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatted {

    public String dateFormatted(Date date, String format){
        if (format == null) {
            format = "yyyy-MM-dd";
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(date);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
