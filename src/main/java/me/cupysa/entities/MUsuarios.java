package me.cupysa.entities;

import me.cupysa.databaseConfig.DatabaseConfigurarion;
import me.cupysa.databaseConfig.tblMUsuarios;

import javax.persistence.*;

@Entity
@Table(name = tblMUsuarios.tableName, schema = DatabaseConfigurarion.databaseSchema)
public class MUsuarios {
    @Id
    @GeneratedValue
    @Column(name = tblMUsuarios.id)
    private Integer id;

    @Column(name = tblMUsuarios.nombreUsuario)
    private String nombreUsuario;

    @Column(name = tblMUsuarios.apellidoNombres)
    private String apellidoNombres;

//    @Column(name = tblMUsuarios.panterior)
//    private String panterior;

//    @Column(name = tblMUsuarios.fallidos)
//    private Integer fallidos;

//    @Column(name = tblMUsuarios.nivel)
//    private Integer nivel;

    @Column(name = tblMUsuarios.contrasenia)
    private String contrasenia;

    @Column(name = tblMUsuarios.fechaExpira)
    private String fechaExpira;

    @Column(name = tblMUsuarios.estado)
    private String estado;

//    @Column(name = tblMUsuarios.panteriorf)
//    private String panteriorf;
//
//    @Column(name = tblMUsuarios.contraseniaf)
//    private String contraseniaf;
//
//    @Column(name = tblMUsuarios.direccion)
//    private String direccion;
//
//    @Column(name = tblMUsuarios.audit)
//    private Integer audit;
//
//    @Column(name = tblMUsuarios.mail)
//    private String mail;
//
//    @Column(name = tblMUsuarios.aviso)
//    private Integer aviso;

//    @Column(name = tblMUsuarios.permisosEspeciales)
//    private String permisosEspeciales;

//    @Column(name = tblMUsuarios.claveCaja)
//    private String claveCaja;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoNombres() {
        return apellidoNombres;
    }

    public void setApellidoNombres(String apellidoNombres) {
        this.apellidoNombres = apellidoNombres;
    }

//    public String getPanterior() {
//        return panterior;
//    }

//    public void setPanterior(String panterior) {
//        this.panterior = panterior;
//    }

//    public Integer getFallidos() {
//        return fallidos;
//    }

//    public void setFallidos(Integer fallidos) {
//        this.fallidos = fallidos;
//    }

//    public Integer getNivel() {
//        return nivel;
//    }

//    public void setNivel(Integer nivel) {
//        this.nivel = nivel;
//    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getFechaExpira() {
        return fechaExpira;
    }

    public void setFechaExpira(String fechaExpira) {
        this.fechaExpira = fechaExpira;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

//    public String getPanteriorf() {
//        return panteriorf;
//    }

//    public void setPanteriorf(String panteriorf) {
//        this.panteriorf = panteriorf;
//    }

//    public String getContraseniaf() {
//        return contraseniaf;
//    }

//    public void setContraseniaf(String contraseniaf) {
//        this.contraseniaf = contraseniaf;
//    }

//    public String getDireccion() {
//        return direccion;
//    }

//    public void setDireccion(String direccion) {
//        this.direccion = direccion;
//    }

//    public Integer getAudit() {
//        return audit;
//    }

//    public void setAudit(Integer audit) {
//        this.audit = audit;
//    }
//
//    public String getMail() {
//        return mail;
//    }
//
//    public void setMail(String mail) {
//        this.mail = mail;
//    }
//
//    public Integer getAviso() {
//        return aviso;
//    }
//
//    public void setAviso(Integer aviso) {
//        this.aviso = aviso;
//    }
//
//    public String getPermisosEspeciales() {
//        return permisosEspeciales;
//    }
//
//    public void setPermisosEspeciales(String permisosEspeciales) {
//        this.permisosEspeciales = permisosEspeciales;
//    }
//
//    public String getClaveCaja() {
//        return claveCaja;
//    }
//
//    public void setClaveCaja(String claveCaja) {
//        this.claveCaja = claveCaja;
//    }


}
