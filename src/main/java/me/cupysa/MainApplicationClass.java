package me.cupysa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class MainApplicationClass extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MainApplicationClass.class);
    }

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(
                MainApplicationClass.class, args);
        springApplication.run(args);
    }
}

//@SpringBootApplication
//public class MainApplicationClass {
//
//    public static void main(String[] args) {
//        SpringApplication.run(MainApplicationClass.class, args);
//    }
//}