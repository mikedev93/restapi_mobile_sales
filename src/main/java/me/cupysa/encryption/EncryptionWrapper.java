package me.cupysa.encryption;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public class EncryptionWrapper {

    public String encryptEndpoint(String endpoint) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        EncryptionAlgorithm encryptionAlgorithm = new EncryptionAlgorithm();
        return encryptionAlgorithm.SHA1(endpoint);
    }
}
