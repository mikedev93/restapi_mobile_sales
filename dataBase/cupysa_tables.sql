CREATE SCHEMA cupysa;

use cupysa;

CREATE TABLE `tblCortes` (
  `idCorte` int(11) NOT NULL,
  `corteCodigo` varchar(45) NOT NULL,
  `corteNombre` varchar(45) NOT NULL,
  `corteEstado` bit(1) NOT NULL DEFAULT b'1',
  `corteMercado` varchar(45) NOT NULL,
  PRIMARY KEY (`idCorte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblGarrones` (
  `idgarron` int(11) NOT NULL,
  `garronSecuencia` varchar(45) NOT NULL,
  `garronFechaFaena` date NOT NULL,
  `garronNAnimal` varchar(45) NOT NULL COMMENT '	',
  `garronNCaravana` varchar(45) NOT NULL,
  `garronNCarcaza` varchar(45) NOT NULL,
  `garronPDesangrado` varchar(45) NOT NULL,
  `garronPCanalCaliente` varchar(45) NOT NULL,
  `garronPCanalFrio` varchar(45) NOT NULL,
  `garronTipificacion` varchar(45) NOT NULL,
  `garronPh` varchar(45) NOT NULL,
  `garronTemperatura` varchar(45) NOT NULL,
  `garronIdFuncionario` int(11) NOT NULL,
  `garronIdPlanta` int(11) NOT NULL,
  PRIMARY KEY (`idgarron`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblMediciones` (
  `idmedicion` int(11) NOT NULL,
  `medicionIdGarron` int(11) NOT NULL,
  `medicionIdTMedicion` int(11) NOT NULL,
  `medidicionDato` varchar(100) NOT NULL,
  PRIMARY KEY (`idmedicion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblMercados` (
  `idMercado` int(11) NOT NULL,
  `mercadoNombre` varchar(45) NOT NULL,
  `mercadoPais` varchar(45) NOT NULL,
  `mercadoEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idMercado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblPlantas` (
  `idPlanta` int(11) NOT NULL,
  `plantaCodigo` varchar(45) NOT NULL,
  `plantaNombre` varchar(80) NOT NULL,
  `plantaEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idPlanta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblRendimientos` (
  `idrendimiento` int(11) NOT NULL,
  `rendimientoIdGarron` int(11) NOT NULL,
  `rendimientoIdCorte` int(11) NOT NULL,
  `rendimientoPeso` int(11) NOT NULL,
  PRIMARY KEY (`idrendimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblTipoMedicion` (
  `idtmedicion` int(11) NOT NULL,
  `tmedicionNombre` varchar(45) NOT NULL,
  `tmedicionDetalle` varchar(45) NOT NULL,
  `tmedicionEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idtmedicion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tblUsuarios` (
  `idUsuario` int(11) NOT NULL,
  `usuarioUser` varchar(45) NOT NULL,
  `usuarioPass` varchar(45) NOT NULL,
  `usuarioFuncionario` varchar(80) NOT NULL,
  `usuarioEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
