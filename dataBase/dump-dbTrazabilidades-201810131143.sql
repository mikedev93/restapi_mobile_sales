-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 10.0.98.63    Database: dbTrazabilidades
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.17-MariaDB-10.2.17+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblCortes`
--

DROP TABLE IF EXISTS `tblCortes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCortes` (
  `idCorte` int(11) NOT NULL AUTO_INCREMENT,
  `corteCodigo` varchar(45) NOT NULL,
  `corteNombre` varchar(45) NOT NULL,
  `corteEstado` bit(1) NOT NULL DEFAULT b'1',
  `corteIdMercado` int(11) NOT NULL,
  PRIMARY KEY (`idCorte`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblGarrones`
--

DROP TABLE IF EXISTS `tblGarrones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblGarrones` (
  `idgarron` int(11) NOT NULL,
  `garronSecuencia` varchar(45) NOT NULL,
  `garronFechaFaena` date NOT NULL,
  `garronNAnimal` int(11) NOT NULL COMMENT '	',
  `garronNCaravana` varchar(45) NOT NULL,
  `garronNCarcaza` varchar(45) NOT NULL,
  `garronPDesangrado` decimal(10,0) NOT NULL,
  `garronPCanalCaliente` decimal(10,0) NOT NULL,
  `garronPCanalFrio` decimal(10,0) NOT NULL,
  `garronTipificacion` varchar(45) NOT NULL,
  `garronPh` varchar(45) NOT NULL,
  `garronTemperatura` varchar(45) NOT NULL,
  `garronIdFuncionario` int(11) NOT NULL,
  `garronIdPlanta` int(11) NOT NULL,
  PRIMARY KEY (`idgarron`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblMediciones`
--

DROP TABLE IF EXISTS `tblMediciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblMediciones` (
  `idmedicion` int(11) NOT NULL,
  `medicionIdGarron` int(11) NOT NULL,
  `medicionIdTMedicion` int(11) NOT NULL,
  `medidicionDato` decimal(10,0) NOT NULL,
  PRIMARY KEY (`idmedicion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblMercados`
--

DROP TABLE IF EXISTS `tblMercados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblMercados` (
  `idMercado` int(11) NOT NULL,
  `mercadoNombre` varchar(45) NOT NULL,
  `mercadoPais` varchar(45) NOT NULL,
  `mercadoEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idMercado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblPlantas`
--

DROP TABLE IF EXISTS `tblPlantas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPlantas` (
  `idPlanta` int(11) NOT NULL,
  `plantaCodigo` varchar(45) NOT NULL,
  `plantaNombre` varchar(80) NOT NULL,
  `plantaEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idPlanta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblRendimientos`
--

DROP TABLE IF EXISTS `tblRendimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblRendimientos` (
  `idrendimiento` int(11) NOT NULL,
  `rendimientoIdGarron` int(11) NOT NULL,
  `rendimientoIdCorte` int(11) NOT NULL,
  `rendimientoPeso` decimal(11,0) NOT NULL,
  PRIMARY KEY (`idrendimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblTipoMedicion`
--

DROP TABLE IF EXISTS `tblTipoMedicion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTipoMedicion` (
  `idtmedicion` int(11) NOT NULL AUTO_INCREMENT,
  `tmedicionNombre` varchar(45) NOT NULL,
  `tmedicionDetalle` varchar(45) NOT NULL,
  `tmedicionEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idtmedicion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblUsuarios`
--

DROP TABLE IF EXISTS `tblUsuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUsuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuarioUser` varchar(45) NOT NULL,
  `usuarioPass` varchar(45) NOT NULL,
  `usuarioFuncionario` varchar(80) NOT NULL,
  `usuarioEstado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'dbTrazabilidades'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-13 11:43:18
